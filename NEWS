release/002.002 (2017-12-31)

    Make the file /etc/audisp/audispd.conf a template file so we can
    override the overflow action. Add the parameter
    audit::auditd::overflow_action in support of this. (adamhl)

release/002.001 (2017-11-01)

    Due to the changes to the parameters and some heavy refactoring,
    making this a major release.

    Make changes to parameters and separated audisp-simplify code into a
    separate class. Re-factor to allow the non-use of the logsink server.
    (adamhl)

release/001.015 (2017-10-25)

    Add use_logsink_server parameter so that servers using Splunk, ELK,
    or its like will have the option to not send log files to the logsink
    server. (adamhl)

release/001.014 (2016-03-11)

    When ensuring 'absent' also remove the audisp-simplify newsyslog
    configuration. (adamhl)

release/001.013 (2015-10-20)

    Changing module 'absent' behavior: purge packages so that
    config files are removed, as well as ensuring absent a cron.d
    job that is put in place by this module (jlent)

release/001.012 (2015-09-23)

    Filter "Audit daemon rotating log files" log entry (akkornel)

    Add default audit.rules for 32-bit systems and push out
    accordingly (jlent)

release/001.011 (2015-09-09)

    Removing MORE duplicate entries from audit.rules (/etc/ssh,
    /etc/shadow) (jlent)

    Remove BIND and CONNECT from default audit.rules (jlent)

    Allow passage of simplify.ignores file as parameter (jlent)

release/001.010 (2015-08-03)

    Disable 'simplify' mode on RHEL/CentOS5, regardless of
    setting override, due to unmet dependency of
    perl-POSIX-strptime (would have to edit the 3rd party
    auditd simplify perl code to make compatible) (jlent)

release/001.009 (2015-07-31)

    Remove ANOTHER duplicate line from audit.rules (adamhl)

release/001.008 (2015-07-31)

    Remove a duplicate line from audit.rules (adamhl)

release/001.007 (2015-07-29)

    Add a filter-syslog file on wheezy machines to ignore complaints about
    innocuous file permissions on audispd. (adamhl)

release/001.006 (2015-03-24)

    Allow the specification of custom rules as a file or a
    template. (whm)

release/001.005 (2015-02-25)

    Remove use of auditd message protocol.  Use syslog to send audit
    data to central logging host.  (whm)

release/001.004 (2015-02-25)

    Remove file inadvertently left in rsyslog.d.  (whm)

release/001.003 (2015-02-25)

    Manage the audisp-simplify log with newsyslog.  (whm)

release/001.002 (2015-02-17)

    Change the default to generate /var/log/audisp-simplify output
    and to send the results to the logsink server.  (whm)

release/001.001 (2015-02-17)

    Initial release of auditd puppet support.  (whm)
