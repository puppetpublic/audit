# Enable the audisp plugin "audisp-simplify". This plugin sends a
# simplified version of the audit logs to /var/log/audisp-simplify.
#
# The 'simplify' plugin has a dependency of perl-POSIX-strptime that is
# not met on RHEL/CentOS5. In this case, do nothing.


class audit::auditd::simplify (
  $ensure             = present,
  $use_logsink_server = true,
) {

  $afile = 'puppet:///modules/audit'
  $bfile = 'puppet:///modules/base/syslog'

  # Force absent for RHEL/CentOS5
  if ($::osfamily == 'RedHat' and $::lsbmajdistrelease == '5') {
    $ensure_real = absent
  } else {
    $ensure_real = $ensure
  }

  if ($ensure_real == present) {
    ## PRESENT
    package {'stanford-auditd-tools': ensure => installed }

    # Install the file that enables the audisp plugin 'audisp-simplify'
    file { '/etc/audisp/plugins.d/simplify.conf':
      source  => "$afile/etc/audisp/plugins.d/simplify.conf",
      require => Package['auditd'],
    }

    # Make sure /var/log/audisp-simplify is rotated.
    file { '/etc/newsyslog.daily/audisp-simplify':
      mode    => '0644',
      source  => "$afile/etc/newsyslog.daily/audisp-simplify",
      require => Package['newsyslog'],
    }

    # Are we sending logs to logsink server? If so, we have some
    # setting up to do.
    #
    # 05-modules-imfile.conf: tell rsyslogd to load the "file-reader"
    #                         module, that is, treat the lines of a text file
    #                         like syslog messages.
    # 15-input-simplify.conf: read lines from /var/log/audisp-simplify as
    #                         syslog messages tagging each line with
    #                         "audispSimplify".
    # 40-simplify.conf:       each message tagged with "audispSimplify"
    #                         should be forwarded to the log-sink server.
    if ($use_logsink_server) {
      # USING LOGSINK SERVER
      $syslog_tag = 'audispSimplify'
      base::syslog::fragment {
        '05-modules-imfile.conf':
          ensure  => 'present',
          source  => "$bfile/etc/rsyslog.d/05-modules-imfile.conf";
        '15-input-simplify.conf':
          ensure  => 'present',
          content => template('audit/etc/rsyslog.d/15-input-simplify.conf.erb');
        '40-simplify.conf':
          ensure  => 'present',
          content => template('audit/etc/rsyslog.d/40-simplify.conf.erb');
      }
    } else {
      # NOT USING LOGSINK SERVER
      base::syslog::fragment {
        '05-modules-imfile.conf': ensure => 'absent';
        '15-input-simplify.conf': ensure => 'absent';
        '40-simplify.conf':       ensure => 'absent';
      }
    }
  } else {
    ## ABSENT
    if ($::osfamily == 'RedHat' and $::lsbmajdistrelease == '5') {
      # Do nothing - package is not available in repos
    } else {
      package { 'stanford-auditd-tools': ensure => absent }
    }

    # Remove files that setup audisp-simplify:
    file {
      '/etc/audisp/plugins.d/simplify.conf':  ensure => absent;
      '/etc/newsyslog.daily/audisp-simplify': ensure => absent;
      '/etc/audisp/simplify.ignores':         ensure => absent;
    }

    # Remove any rsyslog files related to audisp-simplify:
    base::syslog::fragment {
      '05-modules-imfile.conf': ensure => 'absent';
      '15-input-simplify.conf': ensure => 'absent';
      '40-simplify.conf':       ensure => 'absent';
    }
  }
}
