##############################################################################
# System auditing using auditd
##############################################################################
#
# This module configures auditd to record critical events on a Linux
# system and send the record of the events to a central logging
# server.  The connection to the central server is protected by
# GSSAPI.
#
# For the central logging server the sink_server variable should be set to
# true.  For the client servers server no configuration is required as long
# as the sink server default is acceptable.
#
# Example client server:
#
#  audit::auditd { "{$::hostname}.stanford.edu": ensure => present }
#
# $use_logsink_server: set to true if you want to forward auditd logs to the
#   legacy log-sink server, false otherwise. Eventually, the log-sink
#   server will go away as we are moving to Splunk and ELK.
# Default: true
#
# $simplify: enable the audisp plugin "audisp-simplify". This sends a
#   simplified version of the audit logs to /var/log/audisp-simplify. If
#   $simplify is set to true and $use_logsink_server is also set to true,
#   rsyslog will forward lines from /var/log/audisp-simplify to the
#   logsink server.
# Default: true
#
# $overflow_action: What to do if the audispd process queue gets full. The
#   default is 'SYSLOG' which sends error messages to /var/log/syslog. Howver,
#   this can in cause the disk to get full with these messages, so to avoid
#   this, you can set the value to 'IGNORE'.
# Default: 'SYSLOG'


define audit::auditd (
  $content             = 'NONE',
  $source              = 'NONE',
  $client_source_port  = '650',
  $max_log_file        = 1000,
  $max_log_file_action = 'ROTATE',
  $num_logs            = 5,
  $simplify            = true,
  $syslog_server       = 'logsink.stanford.edu',
  $use_logsink_server  = true,
  $space_left          = 5000,
  $space_left_action   = 'SYSLOG',
  $overflow_action     = 'SYSLOG',
  $ensure
) {

  # An attempt to make the manifest more readable
  $afile = 'puppet:///modules/audit'
  $bfile = 'puppet:///modules/base/syslog'

  case $ensure {

    'present': {
      package {
        'auditd':          ensure => installed;
        'audispd-plugins': ensure => installed;
      }

      # What to audit
      if $content == 'NONE' {
        if $source == 'NONE' {
          # Rules implying 64-bit addressing cause errors on 32-bit systems
          if ($::architecture == 'x86_64' or $::architecture == 'amd64') {
            $src_rules = "$afile/etc/audit/audit.rules.x86_64"
          } else {
            $src_rules = "$afile/etc/audit/audit.rules.i386"
          }
        } else {
          $src_rules = $source
        }
        file { '/etc/audit/audit.rules':
          source  => $src_rules,
          require => Package['auditd'],
        }
      } else {
        file { '/etc/audit/audit.rules':
          content => template($content),
          require => Package['auditd'],
        }
      }
      file { '/etc/audit/auditd.conf':
        content => template('audit/etc/audit/auditd.conf.erb'),
        require => Package['auditd'],
      }

      # Where to send the audit logs.
      file { '/etc/audisp/audispd.conf':
        content => template('audit/etc/audisp/audispd.conf.erb'),
        require => Package['auditd'];
      }

      # Setup audisp-simplify.
      if ($simplify) {
        class { 'audit::auditd::simplify':
          ensure             => present,
          use_logsink_server => $use_logsink_server,
        }
      } else {
        class { 'audit::auditd::simplify':
          ensure => absent,
        }
      }

      # Set up rsyslog to forward auditd messages to the log-sink server
      # (or not).
      if ($use_logsink_server) {

        # In order that rsyslog sees auditd messages, we send them to
        # syslog. The following audisp plugin does that:
        file {'/etc/audisp/plugins.d/syslog.conf':
          source  => "$afile/etc/audisp/plugins.d/syslog.conf",
          require => Package['auditd'],
        }

        base::syslog::fragment { '50-audisp-remote.conf':
            ensure  => 'present',
            content => template('audit/etc/rsyslog.d/50-audisp-remote.conf.erb'),
        }
      } else {
        base::syslog::fragment { '50-audisp-remote.conf':
          ensure => 'absent',
        }
        file {'/etc/audisp/plugins.d/syslog.conf':
          ensure => absent,
        }
      }

      # There appears to be a memory leak that is causing auditd to
      # consume swap space.  Restarting once a week fixes this.
      file { '/etc/cron.d/auditd-restart':
        ensure => present,
        source => 'puppet:///modules/audit/etc/cron.d/auditd-restart',
      }

      # On wheezy we want to ignore complaints about audispd's permissions.
      if ($::lsbdistcodename == 'wheezy') {
        file { '/etc/filter-syslog/auditd-wheezy':
          ensure => present,
          source => 'puppet:///modules/audit/etc/filter-syslog/auditd-wheezy',
        }
      }
    }

    'absent': {
      package {
        'auditd':          ensure => purged;
        'audispd-plugins': ensure => purged;
      }

      file {
        '/etc/audit/auditd.keytab':         ensure => absent;
        '/etc/audit/audit.rules':           ensure => absent;
        '/etc/audit/auditd.conf':           ensure => absent;
        '/etc/audisp/audispd.conf':         ensure => absent;
        '/etc/filter-syslog/auditd-wheezy': ensure => absent;
        '/etc/cron.d/auditd-restart':       ensure => absent;
      }

      # In case 50-audisp-remote.conf got installed, remove it:
      base::syslog::fragment { '50-audisp-remote.conf':
          ensure => 'absent',
      }

      # Make sure that nothing related to audisp-simplify is installed.
      class { 'audit::auditd::simplify':
        ensure => absent,
      }

    }

    default: {
      fail('Call to audit::auditd does not include ensure')
    }
  }
}
